const express = require('express')
const fs = require('fs')
const { exit } = require('process')
const dados = require('./dados.json')
const router = express.Router()

router.get('/mythology/random', function(req, res){
    let random = pegaRandom()
    res.json(random)
})

router.get('/mythology/:id', function(req, res){
    let retorno = filtroMitologia(req.params.id)
    res.json(retorno)
})

router.get('/mythology/gods/:id/:godid', function(req, res){
    let limite = req.query.limite || 1
    let mitologia_id = req.params.id
    let deus_id = req.params.godid
    let retorno = filtroMitologia(mitologia_id)
    res.json(retorno.deuses[deus_id])
})

router.get('/mythology/creatures/:id/:creatureid', function(req, res){
    let limite = req.query.limite || 1
    let mitologia_id = req.params.id
    let criatura_id = req.params.creatureid
    let retorno = filtroMitologia(mitologia_id)
    res.json(retorno.criaturas[criatura_id])
})

router.delete('/mythology/gods/:id/:godid', function(req, res){
    let mitologia_id = req.params.id
    let deus_id = req.params.godid
    
    let obj = dados.mitologia[mitologia_id].deuses.findIndex(v => v.id == deus_id);
    dados.mitologia[mitologia_id].deuses.splice(obj, obj >= 0 ? 1 : 0);
    fs.writeFile("./dados.json", JSON.stringify(dados), function(err) {
        if (err) {
            console.log(err)
        }else{
            res.send('Deus removido com sucesso!')
        }
    });
})

router.delete('/mythology/creatures/:id/:creatureid', function(req, res){
    let mitologia_id = req.params.id
    let criatura_id = req.params.creatureid
    
    let obj = dados.mitologia[mitologia_id].criaturas.findIndex(v => v.id == criatura_id);
    dados.mitologia[mitologia_id].criaturas.splice(obj, obj >= 0 ? 1 : 0);
    fs.writeFile("./dados.json", JSON.stringify(dados), function(err) {
        if (err) {
            console.log(err)
        }else{
            res.send('Criatura removida com sucesso!')
        }
    });
})

router.post('/mythology/gods/:id', function(req, res){
    let mitologia_id = req.params.id
    let deus = req.body
    let higherGodId = 0
    dados.mitologia[mitologia_id].deuses.forEach(god =>{
        higherGodId = god.id > higherGodId ? god.id : higherGodId
    })    
    deus.id = higherGodId + 1
    dados.mitologia[mitologia_id].deuses.push(deus)
    higherGodId = 0
    fs.writeFile("./dados.json", JSON.stringify(dados), function(err) {
        if (err) {
            console.log(err)
        }else{
            res.send('Deus inserido com sucesso!')
        }
    });
})

router.post('/mythology/creatures/:id', function(req, res){
    let mitologia_id = req.params.id
    let criatura = req.body
    let higherCreatureId = 0
    dados.mitologia[mitologia_id].criaturas.forEach(criatura =>{
        if(criatura['id'] > higherCreatureId)
        higherCreatureId = criatura['id']
    })    
    criatura.id = higherCreatureId + 1
    dados.mitologia[mitologia_id].criaturas.push(criatura)
    higherCreatureId = 0
    fs.writeFile("./dados.json", JSON.stringify(dados), function(err) {
        if (err) {
            console.log(err)
        }else{
            res.send('Criatura inserida com sucesso!')
        }
    });
})

router.put('/mythology/gods/:id/:godid', function(req, res){
    let mitologia_id = req.params.id
    let deus_id = req.params.godid
    let deus = req.body
    let index = 0
    Object.keys(dados.mitologia[mitologia_id].deuses).forEach(function(k){
        if(deus_id == dados.mitologia[mitologia_id].deuses[k].id)
        index = k
    });
    dados.mitologia[mitologia_id].deuses[index] = {
        "id" : dados.mitologia[mitologia_id].deuses[index].id,
        "nome": deus.nome ? deus.nome : dados.mitologia[mitologia_id].deuses[index].nome,
        "descricao": deus.descricao ? deus.descricao : dados.mitologia[mitologia_id].deuses[index].descricao,
        "historia": deus.historia ? deus.historia : dados.mitologia[mitologia_id].deuses[index].historia,
        "imagem": deus.imagem ? deus.imagem : dados.mitologia[mitologia_id].deuses[index].imagem
    }
    fs.writeFile("./dados.json", JSON.stringify(dados), function(err) {
        if (err) {
            console.log(err)
        }else{
            res.send('Deus atualizado com sucesso!')
        }
    });
})

router.put('/mythology/creatures/:id/:creatureid', function(req, res){
    let mitologia_id = req.params.id
    let criatura_id = req.params.creatureid
    let criatura = req.body
    let index = 0
    Object.keys(dados.mitologia[mitologia_id].criaturas).forEach(function(k){
        if(criatura_id == dados.mitologia[mitologia_id].criaturas[k].id)
        index = k
    });
    dados.mitologia[mitologia_id].criaturas[index] = {
        "id" : dados.mitologia[mitologia_id].criaturas[index].id,
        "nome": criatura.nome ? criatura.nome : dados.mitologia[mitologia_id].criaturas[index].nome,
        "descricao": criatura.descricao ? criatura.descricao : dados.mitologia[mitologia_id].criaturas[index].descricao,
        "historia": criatura.historia ? criatura.historia : dados.mitologia[mitologia_id].criaturas[index].historia,
        "imagem": criatura.imagem ? criatura.imagem : dados.mitologia[mitologia_id].criaturas[index].imagem
    }
    fs.writeFile("./dados.json", JSON.stringify(dados), function(err) {
        if (err) {
            console.log(err)
        }else{
            res.send('Criatura atualizada com sucesso!')
        }
    });
})



function pegaRandom(){
    let random = Math.floor(Math.random()* dados.mitologia.length)
    return dados.mitologia[random]
}

function filtroMitologia(id) {
    return dados.mitologia[id]
}


module.exports = router