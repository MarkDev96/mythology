const express = require('express')
var cors = require('cors')
const routes = require('./routes.js')
const PORT = process.env.PORT || 5000
const app = express()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true}))
app.use('/api' , routes)

app.listen(PORT, function(){
    console.log(`Servidor rodando na porta ${PORT}`)
})