Tipos de rota:

/mythology/random               = retorna mitologia aleatória
/mythology/:mitologia           = retorna mitologia descrita    
/mythology/:mitologia/:deus     = retorna deuses descritos podendo limitar

Rota de exemplo implementada no heroku: https://recuperacao-ppw2.herokuapp.com/api/mythology/egípcia

Repositório: https://gitlab.com/MarkDev96/mythology

